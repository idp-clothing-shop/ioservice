package shop.io.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shop.io.dto.ProductDTO;
import shop.io.dto.ProductDTOMapper;
import shop.io.exceptions.ConflictException;
import shop.io.exceptions.NotFoundException;
import shop.io.model.Cart;
import shop.io.model.CartItem;
import shop.io.model.Product;
import shop.io.repository.CartItemRepository;
import shop.io.repository.ProductRepository;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductService {

	private final ProductRepository productRepository;
	private final ProductDTOMapper productDTOMapper;
	private final CartItemRepository cartItemRepository;

	@Autowired
	public ProductService(ProductRepository productRepository,
			ProductDTOMapper productDTOMapper, CartItemRepository cartItemRepository) {
		this.productRepository = productRepository;
		this.productDTOMapper = productDTOMapper;
		this.cartItemRepository = cartItemRepository;
	}

	public List<ProductDTO> getProducts() {
		return productRepository.findAll()
				.stream()
				.map(productDTOMapper)
				.collect(Collectors.toList());
	}

	public void addNewProduct(Product product) {
		boolean exists = productRepository.existsById(product.getId());
		if (exists) {
			throw new ConflictException("a product with id " + product.getId() + " already exists");
		}
		productRepository.save(product);
	}

	@Transactional
	public void deleteProduct(Long productId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundException(
						"product with id " + productId + " does not exist"));

		cartItemRepository.deleteByProduct(productId);
		productRepository.delete(product);
	}

	@Transactional
	public void updateProduct(Long productId,
								String name,
								Float price,
								String material,
								String image,
								Product.Gender gender) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundException(
						"product with id " + productId + " does not exist"));

		if (name != null &&
				name.length() > 0 &&
				!Objects.equals(product.getName(), name)) {
			product.setName(name);
		}

		if (price != null &&
				!Objects.equals(product.getPrice(), price)) {
			product.setPrice(price);
		}

		if (material != null &&
				material.length() > 0 &&
				!Objects.equals(product.getMaterial(), material)) {
			product.setMaterial(material);
		}

		if (image != null &&
				image.length() > 0 &&
				!Objects.equals(product.getImage(), image)) {
			product.setImage(image);
		}

		if (gender != null &&
				gender != product.getGender()) {
			product.setGender(gender);
		}
	}

	public ProductDTO getProduct(Long productId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundException(
						"product with id " + productId + " does not exist"));
		return productDTOMapper.apply(product);
	}
}
