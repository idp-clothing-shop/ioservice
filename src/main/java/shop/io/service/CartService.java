package shop.io.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.io.exceptions.NotFoundException;
import shop.io.model.Cart;
import shop.io.model.CartItem;
import shop.io.model.Product;
import shop.io.model.User;
import shop.io.repository.CartItemRepository;
import shop.io.repository.CartRepository;
import shop.io.repository.ProductRepository;
import shop.io.repository.UserRepository;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class CartService {

	private final CartRepository cartRepository;
	private final CartItemRepository cartItemRepository;
	private final UserRepository userRepository;
	private final ProductRepository productRepository;

	@Transactional
	private Cart searchCart(Long userId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
						"User with id " + userId + " does not exist"));

		Cart cart = cartRepository.findByUser(user);

		if (cart == null) {
			cart = new Cart(user);
			cartRepository.save(cart);
			user.setCart(cart);
		}

		return cart;
	}

	public Set<CartItem> getCart(Long userId) {
		Cart cart = searchCart(userId);
		return cart.getItems();
	}

	@Transactional
	public void addToCart(Long productId,
			Long userId,
			CartItem.Size size,
			Integer quantity) {

		Cart cart = searchCart(userId);

		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
						"Product with id " + productId + " does not exist"));

		if (quantity == null || quantity.equals(0)) {
			quantity = 1;
		}

		Set<CartItem> cartItems = cart.getItems();
		for (CartItem item : cartItems) {
			if (item.getProduct().getId().equals(productId)
					&& item.getSize().equals(size)) {

				item.setQuantity(item.getQuantity() + quantity);
				return;
			}
		}

		CartItem newItem = new CartItem(product, size, cart, quantity);
		cartItemRepository.save(newItem);
	}

	public void removeItem(Long userId, Long itemId) {
		CartItem item = cartItemRepository.findById(itemId)
				.orElseThrow(() -> new ResponseStatusException(
						HttpStatus.NOT_FOUND,
						"Item not found in cart"));
		Cart cart = item.getCart();
		if (!cart.getUser().getId().equals(userId)) {
			throw new ResponseStatusException(
					HttpStatus.NOT_FOUND,
					"Item not found in cart");
		}

		cart.getItems().remove(item);
		cartItemRepository.deleteById(itemId);
	}
}
