package shop.io.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "addresses")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String city;
	@Column(nullable = false)
	private String street;
	@Column(name = "street_number", nullable = false)
	private Long streetNumber;
}
