package shop.io.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Order {

	public enum OrderStatus {
		PROCESSING, SHIPPED, DELIVERED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "address_id", referencedColumnName = "id", nullable = false)
	private Address address;

	@OneToMany(mappedBy = "order",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	private Set<OrderItem> items = new HashSet<>();

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "VARCHAR(15) DEFAULT 'PROCESSING'")
	private OrderStatus status;

	@Column(name = "placement_time", columnDefinition = "TIMESTAMP DEFAULT LOCALTIMESTAMP")
	public LocalDateTime placementTime;
}
