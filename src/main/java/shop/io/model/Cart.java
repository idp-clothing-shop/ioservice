package shop.io.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "carts")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	public Cart(User user) {
		this.user = user;
	}

	@OneToMany(mappedBy = "cart",
				cascade = CascadeType.ALL,
				fetch = FetchType.LAZY,
				orphanRemoval = true)
	private Set<CartItem> items = new HashSet<>();
}
