package shop.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "order_items",
		uniqueConstraints = {
				@UniqueConstraint(name = "UniqueOrderItem",
						columnNames = {"order_id", "product_id", "size"})
		})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

	public enum Size {
		XXS, XS, S, M, L, XL, XXL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false)
	private Product product;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Size size;

	@ManyToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			optional = false)
	@JoinColumn(name = "order_id", referencedColumnName = "id", nullable = false)
	@JsonIgnore
	private Order order;

	@Column(nullable = false)
	private int quantity;
}
