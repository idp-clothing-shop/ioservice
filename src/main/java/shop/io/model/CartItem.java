package shop.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "cart_items",
		uniqueConstraints = {
				@UniqueConstraint(name = "UniqueCartItem",
						columnNames = {"cart_id", "product_id", "size"})
		})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartItem {

	public enum Size {
		XXS, XS, S, M, L, XL, XXL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false)
	private Product product;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Size size;

	@ManyToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			optional = false)
	@JoinColumn(name = "cart_id", referencedColumnName = "id", nullable = false)
	@JsonIgnore
	private Cart cart;

	@Column(nullable = false)
	private int quantity;

	public CartItem(Product product,
					Size size,
					Cart cart,
					int quantity) {
		this.product = product;
		this.size = size;
		this.cart = cart;
		this.quantity = quantity;
	}
}
