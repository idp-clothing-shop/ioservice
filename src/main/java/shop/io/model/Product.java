package shop.io.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "products")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {

	public enum Gender {
		WOMAN,
		MAN
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private Float price;
	private String material;
	private String image;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Gender gender;

	public Product(String name,
					Float price,
					String material,
					Gender gender) {
		this.name = name;
		this.price = price;
		this.material = material;
		this.gender = gender;
	}
}
