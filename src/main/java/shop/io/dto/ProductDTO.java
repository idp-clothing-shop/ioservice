package shop.io.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductDTO {
	private Long id;
	private String name;
	private Float price;
	private String material;
	private String image;
	private String gender;
}
