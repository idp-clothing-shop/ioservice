package shop.io.dto;

import org.springframework.stereotype.Service;
import shop.io.model.Product;

import java.util.function.Function;

@Service
public class ProductDTOMapper implements Function<Product, ProductDTO> {
	@Override
	public ProductDTO apply(Product product) {
		return new ProductDTO(
				product.getId(),
				product.getName(),
				product.getPrice(),
				product.getMaterial(),
				product.getImage(),
				product.getGender().toString()
		);
	}
}
