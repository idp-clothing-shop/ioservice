package shop.io.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.io.model.CartItem;

import java.util.Set;

@Repository
public interface CartItemRepository
		extends JpaRepository<CartItem, Long> {

	@Query("SELECT i FROM CartItem i WHERE i.product.id = ?1")
	Set<CartItem> findByProductId(Long productId);

	@Modifying
	@Transactional
	@Query("DELETE FROM CartItem i WHERE i.product.id = ?1")
	void deleteByProduct(Long productId);
}
