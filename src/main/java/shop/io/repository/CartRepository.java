package shop.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shop.io.model.Cart;
import shop.io.model.CartItem;
import shop.io.model.User;

import java.util.List;

@Repository
public interface CartRepository
		extends JpaRepository<Cart, Long> {

	Cart findByUser(User user);
}
