package shop.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shop.io.model.User;

@Repository
public interface UserRepository
		extends JpaRepository<User, Long> {
}
