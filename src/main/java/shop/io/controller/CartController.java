package shop.io.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import shop.io.model.CartItem;
import shop.io.service.CartService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "api/io/cart")
public class CartController {

	private final CartService cartService;

	@Autowired
	public CartController(CartService cartService) {
		this.cartService = cartService;
	}

	@PostMapping(path = "{productId}")
	public void addToCart(
			@PathVariable("productId") Long productId,
			@RequestParam Long userId,
			@RequestParam CartItem.Size size,
			@RequestParam(required = false) Integer quantity) {
		cartService.addToCart(productId, userId, size, quantity);
	}

	@GetMapping
	public List<CartItem> getCart(
			@RequestParam(required = true) Long userId) {
		return new ArrayList<>(cartService.getCart(userId));
	}

	@DeleteMapping
	public void removeFromCart(
			@RequestParam(value = "user") Long userId,
			@RequestParam(value = "item") Long itemId) {
		cartService.removeItem(userId, itemId);
	}
}
