package shop.io.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import shop.io.dto.ProductDTO;
import shop.io.model.Product;
import shop.io.service.CartService;
import shop.io.service.ProductService;

import java.util.List;

@RestController
@RequestMapping(path = "api/io/products")
public class ProductController {

	private final ProductService productService;
	private final CartService cartService;

	@Autowired
	public ProductController(ProductService productService, CartService cartService) {
		this.productService = productService;
		this.cartService = cartService;
	}

	@GetMapping
	public List<ProductDTO> getProducts() {
		return productService.getProducts();
	}

	@PostMapping
	public void registerNewProduct(@RequestBody Product product) {
		productService.addNewProduct(product);
	}

	@DeleteMapping(path = "{productId}")
	public void deleteProduct(@PathVariable("productId") Long productId) {
		productService.deleteProduct(productId);
	}

	@PutMapping(path = "{productId}")
	public void updateProduct(
			@PathVariable("productId") Long productId,
			@RequestBody Product product) {
		productService.updateProduct(productId,
								product.getName(),
								product.getPrice(),
								product.getMaterial(),
								product.getImage(),
								product.getGender());
	}

	@GetMapping(path = "{productId}")
	public ProductDTO getProduct(@PathVariable("productId") Long productId) {
		return productService.getProduct(productId);
	}
}
