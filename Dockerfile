FROM amazoncorretto:17-alpine-jdk
EXPOSE 8081
COPY target/io-0.0.1.jar io.jar
ENTRYPOINT ["java", "-jar", "io.jar"]
